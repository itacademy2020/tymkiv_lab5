﻿namespace Tymkiv
{
    partial class Tables
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.AirplaneGrid = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.deleteAirplate = new System.Windows.Forms.Button();
            this.updateAirplate = new System.Windows.Forms.Button();
            this.addAirplate = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.deleteFlight = new System.Windows.Forms.Button();
            this.updateFlight = new System.Windows.Forms.Button();
            this.addFlight = new System.Windows.Forms.Button();
            this.flightGrid = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.deleteEmploee = new System.Windows.Forms.Button();
            this.updateEmploee = new System.Windows.Forms.Button();
            this.addEmploee = new System.Windows.Forms.Button();
            this.emploeeGrid = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.deletePassenger = new System.Windows.Forms.Button();
            this.updatePassenger = new System.Windows.Forms.Button();
            this.addPassenger = new System.Windows.Forms.Button();
            this.passengerGrid = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AirplaneGrid)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.flightGrid)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.emploeeGrid)).BeginInit();
            this.tabPage4.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passengerGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 2;
            this.tabControl1.Size = new System.Drawing.Size(994, 543);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.AirplaneGrid);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(986, 510);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Літаки";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // AirplaneGrid
            //// 
            this.AirplaneGrid.AllowUserToAddRows = false;
            this.AirplaneGrid.AllowUserToDeleteRows = false;
            this.AirplaneGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AirplaneGrid.Location = new System.Drawing.Point(9, 13);
            this.AirplaneGrid.Name = "AirplaneGrid";
            this.AirplaneGrid.ReadOnly = true;
            this.AirplaneGrid.RowHeadersWidth = 51;
            this.AirplaneGrid.Size = new System.Drawing.Size(970, 401);
            this.AirplaneGrid.TabIndex = 1;
            this.AirplaneGrid.Text = "dataGridView1";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.deleteAirplate);
            this.groupBox1.Controls.Add(this.updateAirplate);
            this.groupBox1.Controls.Add(this.addAirplate);
            this.groupBox1.Location = new System.Drawing.Point(3, 420);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(980, 85);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " ";
            // 
            // deleteAirplate
            // 
            this.deleteAirplate.Location = new System.Drawing.Point(792, 26);
            this.deleteAirplate.Name = "deleteAirplate";
            this.deleteAirplate.Size = new System.Drawing.Size(182, 46);
            this.deleteAirplate.TabIndex = 0;
            this.deleteAirplate.Text = "Видалити";
            this.deleteAirplate.UseVisualStyleBackColor = true;
            this.deleteAirplate.Click += new System.EventHandler(this.deleteAirplate_Click);
            // 
            // updateAirplate
            // 
            this.updateAirplate.Location = new System.Drawing.Point(388, 26);
            this.updateAirplate.Name = "updateAirplate";
            this.updateAirplate.Size = new System.Drawing.Size(183, 46);
            this.updateAirplate.TabIndex = 0;
            this.updateAirplate.Text = "Редагувати";
            this.updateAirplate.UseVisualStyleBackColor = true;
            this.updateAirplate.Click += new System.EventHandler(this.updateAirplate_Click);
            // 
            // addAirplate
            // 
            this.addAirplate.Location = new System.Drawing.Point(6, 26);
            this.addAirplate.Name = "addAirplate";
            this.addAirplate.Size = new System.Drawing.Size(167, 46);
            this.addAirplate.TabIndex = 0;
            this.addAirplate.Text = "Додати";
            this.addAirplate.UseVisualStyleBackColor = true;
            this.addAirplate.Click += new System.EventHandler(this.addAirplate_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBox2);
            this.tabPage2.Controls.Add(this.flightGrid);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(986, 510);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Рейси";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.deleteFlight);
            this.groupBox2.Controls.Add(this.updateFlight);
            this.groupBox2.Controls.Add(this.addFlight);
            this.groupBox2.Location = new System.Drawing.Point(3, 420);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(980, 85);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " ";
            // 
            // deleteFlight
            // 
            this.deleteFlight.Location = new System.Drawing.Point(792, 26);
            this.deleteFlight.Name = "deleteFlight";
            this.deleteFlight.Size = new System.Drawing.Size(182, 46);
            this.deleteFlight.TabIndex = 0;
            this.deleteFlight.Text = "Видалити";
            this.deleteFlight.UseVisualStyleBackColor = true;
            this.deleteFlight.Click += new System.EventHandler(this.deleteFlight_Click);
            // 
            // updateFlight
            // 
            this.updateFlight.Location = new System.Drawing.Point(388, 26);
            this.updateFlight.Name = "updateFlight";
            this.updateFlight.Size = new System.Drawing.Size(183, 46);
            this.updateFlight.TabIndex = 0;
            this.updateFlight.Text = "Редагувати";
            this.updateFlight.UseVisualStyleBackColor = true;
            this.updateFlight.Click += new System.EventHandler(this.updateFlight_Click);
            // 
            // addFlight
            // 
            this.addFlight.Location = new System.Drawing.Point(6, 26);
            this.addFlight.Name = "addFlight";
            this.addFlight.Size = new System.Drawing.Size(167, 46);
            this.addFlight.TabIndex = 0;
            this.addFlight.Text = "Додати";
            this.addFlight.UseVisualStyleBackColor = true;
            this.addFlight.Click += new System.EventHandler(this.addFlight_Click);
            // 
            // flightGrid
            // 
            this.flightGrid.AllowUserToAddRows = false;
            this.flightGrid.AllowUserToDeleteRows = false;
            this.flightGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.flightGrid.Location = new System.Drawing.Point(9, 13);
            this.flightGrid.Name = "flightGrid";
            this.flightGrid.ReadOnly = true;
            this.flightGrid.RowHeadersWidth = 51;
            this.flightGrid.Size = new System.Drawing.Size(970, 401);
            this.flightGrid.TabIndex = 1;
            this.flightGrid.Text = "dataGridView1";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox3);
            this.tabPage3.Controls.Add(this.emploeeGrid);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(986, 510);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Працівники";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.deleteEmploee);
            this.groupBox3.Controls.Add(this.updateEmploee);
            this.groupBox3.Controls.Add(this.addEmploee);
            this.groupBox3.Location = new System.Drawing.Point(3, 419);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(976, 85);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " ";
            // 
            // deleteEmploee
            // 
            this.deleteEmploee.Location = new System.Drawing.Point(792, 26);
            this.deleteEmploee.Name = "deleteEmploee";
            this.deleteEmploee.Size = new System.Drawing.Size(182, 46);
            this.deleteEmploee.TabIndex = 0;
            this.deleteEmploee.Text = "Видалити";
            this.deleteEmploee.UseVisualStyleBackColor = true;
            this.deleteEmploee.Click += new System.EventHandler(this.deleteEmploee_Click);
            // 
            // updateEmploee
            // 
            this.updateEmploee.Location = new System.Drawing.Point(388, 26);
            this.updateEmploee.Name = "updateEmploee";
            this.updateEmploee.Size = new System.Drawing.Size(183, 46);
            this.updateEmploee.TabIndex = 0;
            this.updateEmploee.Text = "Редагувати";
            this.updateEmploee.UseVisualStyleBackColor = true;
            this.updateEmploee.Click += new System.EventHandler(this.updateEmploee_Click);
            // 
            // addEmploee
            // 
            this.addEmploee.Location = new System.Drawing.Point(6, 26);
            this.addEmploee.Name = "addEmploee";
            this.addEmploee.Size = new System.Drawing.Size(167, 46);
            this.addEmploee.TabIndex = 0;
            this.addEmploee.Text = "Додати";
            this.addEmploee.UseVisualStyleBackColor = true;
            this.addEmploee.Click += new System.EventHandler(this.addEmploee_Click);
            // 
            // emploeeGrid
            // 
            this.emploeeGrid.AllowUserToAddRows = false;
            this.emploeeGrid.AllowUserToDeleteRows = false;
            this.emploeeGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.emploeeGrid.Location = new System.Drawing.Point(13, 12);
            this.emploeeGrid.Name = "emploeeGrid";
            this.emploeeGrid.ReadOnly = true;
            this.emploeeGrid.RowHeadersWidth = 51;
            this.emploeeGrid.Size = new System.Drawing.Size(970, 401);
            this.emploeeGrid.TabIndex = 1;
            this.emploeeGrid.Text = "dataGridView1";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Controls.Add(this.passengerGrid);
            this.tabPage4.Location = new System.Drawing.Point(4, 29);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(986, 510);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Пасажири";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.deletePassenger);
            this.groupBox4.Controls.Add(this.updatePassenger);
            this.groupBox4.Controls.Add(this.addPassenger);
            this.groupBox4.Location = new System.Drawing.Point(7, 422);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(976, 85);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = " ";
            // 
            // deletePassenger
            // 
            this.deletePassenger.Location = new System.Drawing.Point(792, 26);
            this.deletePassenger.Name = "deletePassenger";
            this.deletePassenger.Size = new System.Drawing.Size(182, 46);
            this.deletePassenger.TabIndex = 0;
            this.deletePassenger.Text = "Видалити";
            this.deletePassenger.UseVisualStyleBackColor = true;
            this.deletePassenger.Click += new System.EventHandler(this.deletePassenger_Click);
            // 
            // updatePassenger
            // 
            this.updatePassenger.Location = new System.Drawing.Point(388, 26);
            this.updatePassenger.Name = "updatePassenger";
            this.updatePassenger.Size = new System.Drawing.Size(183, 46);
            this.updatePassenger.TabIndex = 0;
            this.updatePassenger.Text = "Редагувати";
            this.updatePassenger.UseVisualStyleBackColor = true;
            this.updatePassenger.Click += new System.EventHandler(this.updatePassenger_Click);
            // 
            // addPassenger
            // 
            this.addPassenger.Location = new System.Drawing.Point(6, 26);
            this.addPassenger.Name = "addPassenger";
            this.addPassenger.Size = new System.Drawing.Size(167, 46);
            this.addPassenger.TabIndex = 0;
            this.addPassenger.Text = "Додати";
            this.addPassenger.UseVisualStyleBackColor = true;
            this.addPassenger.Click += new System.EventHandler(this.addPassenger_Click);
            // 
            // passengerGrid
            // 
            this.passengerGrid.AllowUserToAddRows = false;
            this.passengerGrid.AllowUserToDeleteRows = false;
            this.passengerGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.passengerGrid.Location = new System.Drawing.Point(9, 14);
            this.passengerGrid.Name = "passengerGrid";
            this.passengerGrid.ReadOnly = true;
            this.passengerGrid.RowHeadersWidth = 51;
            this.passengerGrid.Size = new System.Drawing.Size(970, 401);
            this.passengerGrid.TabIndex = 1;
            this.passengerGrid.Text = "dataGridView1";
            // 
            // Tables
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1007, 558);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "Tables";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tables";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AirplaneGrid)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.flightGrid)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.emploeeGrid)).EndInit();
            this.tabPage4.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.passengerGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button addAirplate;
        private System.Windows.Forms.Button deleteAirplate;
        private System.Windows.Forms.Button updateAirplate;
        private System.Windows.Forms.DataGridView AirplaneGrid;
        private System.Windows.Forms.DataGridView flightGrid;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button deleteEmploee;
        private System.Windows.Forms.Button updateFlight;
        private System.Windows.Forms.Button addFlight;
        private System.Windows.Forms.Button deleteFlight;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button updateEmploee;
        private System.Windows.Forms.Button addEmploee;
        private System.Windows.Forms.DataGridView emploeeGrid;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button updatePassenger;
        private System.Windows.Forms.Button addPassenger;
        private System.Windows.Forms.DataGridView passengerGrid;
        private System.Windows.Forms.Button deletePassenger;
    }
}