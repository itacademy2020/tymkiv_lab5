﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Tymkiv
{
    public partial class addOrUpdateEmploee : Form
    {
        public Emploee emploee;
        public bool isReady;
        private Emploee oldEmloee;
        public addOrUpdateEmploee()
        {
            InitializeComponent();
        }

        public addOrUpdateEmploee(Emploee emploee)
        {
            InitializeComponent();

            fullNameTextBox.Text = emploee.Full_Name;
            addressTextBox.Text = emploee.addres;
            brthdayDateTimePicker.Value = emploee.brthday;
            dateEmploymentDateTimePicker.Value = emploee.Date_Employment;

            oldEmloee = emploee;
        }

        private void accept_Click(object sender, EventArgs e)
        {
            emploee = new Emploee();
            try
            {
                if (fullNameTextBox.Text.Length > 0)
                    emploee.Full_Name = fullNameTextBox.Text.ToString();
                else
                    throw new ArgumentException("Введіть ім'я працівника");

                if (addressTextBox.Text.Length > 0)
                    emploee.addres = addressTextBox.Text.ToString();
                else
                    throw new ArgumentException("Введіть адрес працівника");

                if (brthdayDateTimePicker.Value < DateTime.Now)
                    emploee.brthday = brthdayDateTimePicker.Value;
                else
                    throw new ArgumentException("Введіть корректну дату народження");

                if (dateEmploymentDateTimePicker.Value > brthdayDateTimePicker.Value)
                    emploee.Date_Employment = dateEmploymentDateTimePicker.Value;
                else
                    throw new ArgumentException("Введіть корректну дату працеплаштування");

                if (oldEmloee != null)
                {
                    emploee.Number_Emploee = oldEmloee.Number_Emploee;
                }
                isReady = true;
                this.Close();
            }
            catch (ArgumentException aex)
            {
                MessageBox.Show(aex.Message);
            }
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void stringTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)e.KeyChar == (Char)Keys.Back) return;
            if (char.IsLetter(e.KeyChar) || (char)e.KeyChar == '-' || (char)e.KeyChar == ' ' || (char)e.KeyChar == '`') return;
            e.Handled = true;
        }

        private void stringAndNumberTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)e.KeyChar == (Char)Keys.Back) return;
            if (char.IsLetterOrDigit(e.KeyChar) || (char)e.KeyChar == '-' || (char)e.KeyChar == ' ' || (char)e.KeyChar == '`' || (char)e.KeyChar == '.') return;
            e.Handled = true;
        }


    }
}
