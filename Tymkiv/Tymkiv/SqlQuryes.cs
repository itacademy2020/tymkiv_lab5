﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Tymkiv
{
    public partial class SqlQuryes : Form
    {

        public const string connectString = "Server=tcp:itacademy.database.windows.net,1433;" +
                                            "Database=Tymkiv;" +
                                            "User ID=Tymkiv;" +
                                            "Password=Hurd93522;" +
                                            "Trusted_Connection=False;Encrypt=True;";

        public SqlQuryes()
        {
            InitializeComponent();
            tabControl1.SelectedIndex = 0;
            InitializeFlightComboBox();
        }

        private void Clear(DataGridView view)
        {
            int rowsCount = view.Rows.Count;
            for (int i = 0; i < rowsCount; i++)
            {
                view.Rows.Remove(view.Rows[0]);
            }
        }


        private void InitializeFlightComboBox()
        {
            Clear(crawFlightGrid);
            Flights flights = new Flights();
            flights.wtriteData();

            flightComboBox.DataSource = flights.GetMinimizes();
            flightComboBox.DisplayMember = "FullFlight";
        }

        private void InitializeAirplaneCityComboBox()
        {
            Clear(crawFlightGrid);
            using (SqlConnection connection = new SqlConnection(connectString))
            {

                string query = "select distinct Place_Arrival from Flight";
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        cityComboBox.Items.Add(reader.GetValue(0).ToString());
                    }
                }
                reader.Close();
                cityComboBox.SelectedIndex = 0;
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl1.SelectedIndex == 0)
            {
                InitializeFlightComboBox();
            }
            if(tabControl1.SelectedIndex == 2)
            {
                InitializeAirplaneCityComboBox();
            }
        }

        private void intTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back) return;
            else
                e.Handled = true;
        }

        private void query1_Click(object sender, EventArgs e)
        {
            int index = flightComboBox.SelectedIndex;
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                string query = String.Format("select PE.Position, E.Full_Name , E.Date_Employment , E.Brthday  " +
                    "from Flight F " +
                    "inner join Airplane A " +
                    "on A.Serial_Number = F.Airplane " +
                    "inner join TypePlane TP " +
                    "on TP.ID = A.Type_Plane " +
                    "inner join Craw C " +
                    "on C.Flight = F.Number_Flight " +
                    "inner join Emploee E " +
                    "on C.Emploee = E.Number_Emploee " +
                    "inner join PositionEmploee PE " +
                    "on PE.ID = C.Position " +
                    "where F.Number_Flight = {0}",index);

                connection.Open();
                SqlDataAdapter DA = new SqlDataAdapter(new SqlCommand(query,connection));
                DataSet DS = new DataSet();
                DA.Fill(DS, "Q1");
                crawFlightGrid.DataSource = DS.Tables["Q1"];
                crawFlightGrid.Refresh();
            }
        }

        private void query2_Click(object sender, EventArgs e)
        {
            int range = 0;
            if (rangeTextBox.Text.Length >= 1)
                range =  int.Parse(rangeTextBox.Text.ToString());
            else
            {
                MessageBox.Show("Введіть дальність польоту!");
                return;
            }    

            using (SqlConnection connection = new SqlConnection(connectString))
            {
                string query = String.Format("select* from Flight F " +
                    "inner join Airplane A " +
                    "on F.Airplane = A.Serial_Number " +
                    "where A.Flight_Range >= {0}", range);

                connection.Open();
                SqlDataAdapter DA = new SqlDataAdapter(new SqlCommand(query, connection));
                DataSet DS = new DataSet();
                DA.Fill(DS, "Q1");
                airplaneGrid.DataSource = DS.Tables["Q1"];
                airplaneGrid.Refresh();
            }
        }

        private void query3_Click(object sender, EventArgs e)
        {
            string city = cityComboBox.SelectedItem.ToString();

            using (SqlConnection connection = new SqlConnection(connectString))
            {
                string query = String.Format("select P.*from Passenger P " +
                    "inner join BuyedTicket BT " +
                    "on p.Number_Document = BT.Passenger " +
                    "inner join Ticket T " +
                    "on BT.Ticket = T.ID " +
                    "inner join Flight F " +
                    "on T.Flight = F.Number_Flight " +
                    "where F.Place_Arrival = '{0}'", city);

                connection.Open();
                SqlDataAdapter DA = new SqlDataAdapter(new SqlCommand(query, connection));
                DataSet DS = new DataSet();
                DA.Fill(DS, "Q1");
                passengerGrid.DataSource = DS.Tables["Q1"];
                passengerGrid.Refresh();
            }
        }
    }
}
