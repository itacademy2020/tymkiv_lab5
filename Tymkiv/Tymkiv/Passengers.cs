﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Tymkiv
{
    class Passengers
    {
        private List<Passenger> passengers;
        private QuerryToDatabase querry;
        public const string connectString = "Server=tcp:itacademy.database.windows.net,1433;" +
                                            "Database=Tymkiv;" +
                                            "User ID=Tymkiv;" +
                                            "Password=Hurd93522;" +
                                            "Trusted_Connection=False;Encrypt=True;";

        public List<Passenger> AllPassangers
        {
            private set { }
            get
            {
                return passengers;
            }
        }

        public Passengers()
        {
            passengers = new List<Passenger>();
            querry = new QuerryToDatabase();
        }

        public bool wtriteData()
        {
            passengers = new List<Passenger>();
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                try
                {
                    string query = "Select * from Passenger";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Passenger passenger = new Passenger();
                            passenger.Number_Document = (int)reader.GetValue(0);
                            passenger.Type_Document = (int)reader.GetValue(1);
                            passenger.name = reader.GetValue(2).ToString();
                            passenger.surname = reader.GetValue(3).ToString();
                            passenger.address = reader.GetValue(4).ToString();
                            passenger.phone = reader.GetValue(5).ToString();
                            passenger.email = reader.GetValue(6).ToString();

                            passengers.Add(passenger);
                        }
                    }
                    reader.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }


        public bool add(Passenger passenger)
        {
            string query = String.Format("INSERT INTO Passenger " +
                        "VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}')",
                        passenger.Type_Document,
                        passenger.name,
                        passenger.surname,
                        passenger.address,
                        passenger.phone,
                        passenger.email);

            return querry.crudOperation(query);
        }

        public bool update(Passenger passenger)
        {
            string query = String.Format("UPDATE Passenger set Type_Document = {0}, name = '{1}', surname = '{2}', address = '{3}', phone = '{4}', email  = '{5}'" +
                "where Number_Document = {6}",
                passenger.Type_Document,
                passenger.name,
                passenger.surname,
                passenger.address,
                passenger.phone,
                passenger.email,
                passenger.Number_Document);

            return querry.crudOperation(query);
        }

        public bool delete(Passenger passenger)
        {
            string query = String.Format("DELETE FROM Passenger WHERE Number_Document = {0}",
                passenger.Number_Document);

            return querry.crudOperation(query);
        }
    }
}
