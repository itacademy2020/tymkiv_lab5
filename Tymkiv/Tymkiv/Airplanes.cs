﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
namespace Tymkiv
{
    class Airplanes
    {

        private List<Airplane> airplanes;
        private QuerryToDatabase querry;
        public const string connectString = "Server=tcp:itacademy.database.windows.net,1433;" +
                                            "Database=Tymkiv;" +
                                            "User ID=Tymkiv;" +
                                            "Password=Hurd93522;" +
                                            "Trusted_Connection=False;Encrypt=True;";

        public List<Airplane> AllAirplanes
        {
            private set { }
            get
            {
                return airplanes;
            }
        }
        public Airplanes()
        {
            airplanes = new List<Airplane>();
            querry = new QuerryToDatabase();
        }

        public bool wtriteData()
        {
            airplanes = new List<Airplane>();
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                try
                {
                    string query = "Select * from Airplane";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Airplane airplane = new Airplane();
                            airplane.Serial_Number = (int)reader.GetValue(0);
                            airplane.Type_Plane = (int)reader.GetValue(1);
                            airplane.Brand = reader.GetValue(2).ToString();
                            airplane.Model = reader.GetValue(3).ToString();
                            airplane.Flight_Range = float.Parse(reader.GetValue(4).ToString());
                            airplane.Capacity = (int)reader.GetValue(5);
                            airplane.Tonnage = float.Parse(reader.GetValue(6).ToString());
                            airplane.Fuel = float.Parse(reader.GetValue(7).ToString());

                            airplanes.Add(airplane);
                        }
                    }
                    reader.Close();
                    return true;
                }
                catch(Exception ex)
                {
                    return false;
                }
            }
        }
 
        public bool add(Airplane airplane)
        {
            string query = String.Format("INSERT INTO Airplane " +
                        "VALUES ({0}, '{1}', '{2}', {3}, {4}, {5}, {6})",
                        airplane.Type_Plane, airplane.Brand, airplane.Model, airplane.Flight_Range, airplane.Capacity, airplane.Tonnage, airplane.Fuel);

            return querry.crudOperation(query);
        }

        public bool delete(Airplane airplane)
        {
            string query = String.Format("DELETE FROM Airplane WHERE Serial_Number = {0}",
                       airplane.Serial_Number);

            return querry.crudOperation(query);
        }

        public bool update(Airplane airplane)
        {
            string query = String.Format("UPDATE Airplane set Type_Plane = {0}, Brand = '{1}', Model = '{2}', Flight_Range = {3}, Capacity = {4}," +
                        "Tonnage = {5}, Fuel = {6} where Serial_Number = {7}",
                        airplane.Type_Plane, airplane.Brand, airplane.Model, airplane.Flight_Range, airplane.Capacity, airplane.Tonnage, airplane.Fuel,
                        airplane.Serial_Number);

            return querry.crudOperation(query);
        }
    }
}
