﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tymkiv
{
    public class Airplane
    {
        public int Serial_Number { get; set; }

        public int Type_Plane { get; set; }

        public string Brand { get; set; }

        public string Model { get; set; }

        public float Flight_Range { get; set; }

        public int Capacity { get; set; }

        public float Tonnage { get; set; }

        public float Fuel { get; set; }


        public string FuelName => $"{Brand} {Model} ";

    }
}
