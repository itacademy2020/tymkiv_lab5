﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Globalization;
using System.Text;

namespace Tymkiv
{
    class Flights
    {
        private List<Flight> flights;
        private QuerryToDatabase querry;
        public const string connectString = "Server=tcp:itacademy.database.windows.net,1433;" +
                                            "Database=Tymkiv;" +
                                            "User ID=Tymkiv;" +
                                            "Password=Hurd93522;" +
                                            "Trusted_Connection=False;Encrypt=True;";

        public List<Flight> AllFlights
        {
            private set { }
            get
            {
                return flights;
            }
        }

        public Flights()
        {
            flights = new List<Flight>();
            querry = new QuerryToDatabase();
        }

        public bool wtriteData()
        {
            flights = new List<Flight>();
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                try
                {
                    string query = "Select * from Flight";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Flight flight = new Flight();
                            flight.Number_Flight = (int)reader.GetValue(0);
                            flight.Date = (DateTime)reader.GetValue(1);
                            flight.Place_Dispatch = reader.GetValue(2).ToString();
                            flight.Place_Arrival = reader.GetValue(3).ToString();
                            var st = reader.GetValue(4);
                            if(!reader.IsDBNull(4))
                                flight.Transit_Airfield = reader.GetValue(4).ToString();
                            flight.Time_Dispatch = (DateTime)reader.GetValue(5);
                            var obj = reader.GetValue(6);
                            if(!reader.IsDBNull(6))
                                flight.Time_Dispatch_Transit = (DateTime)reader.GetValue(6);
                            flight.Time_Arrival = (DateTime)reader.GetValue(7);
                            if (!reader.IsDBNull(8))
                                flight.Time_Arrival_Transit = (DateTime)reader.GetValue(8);
                            flight.Airplane = (int)reader.GetValue(9);
                            flights.Add(flight);
                        }
                    }
                    reader.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public bool add(Flight flight)
        {
            string query = "";

            if(flight.Transit_Airfield != null)
            {
                query = string.Format("INSERT INTO Flight " +
                        "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', {8})",
                        flight.Date.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Place_Dispatch,
                        flight.Place_Arrival,
                        flight.Transit_Airfield,
                        flight.Time_Dispatch.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Time_Dispatch_Transit.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Time_Arrival.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Time_Arrival_Transit.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Airplane);
            }
            else
            {
                query = string.Format("INSERT INTO Flight(Data,Place_Dispatch,Place_Arrival,Time_Dispatch,Time_Arrival,Airplane) " +
                       "VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', {5})",
                       flight.Date.ToString("yyy-MM-dd HH:mm:ss.fff"),
                       flight.Place_Dispatch,
                       flight.Place_Arrival,
                       flight.Time_Dispatch.ToString("yyy-MM-dd HH:mm:ss.fff"),
                       flight.Time_Arrival.ToString("yyy-MM-dd HH:mm:ss.fff"),
                       flight.Airplane);
            }
            return querry.crudOperation(query);
        }

        public bool delete(Flight flight)
        {
            string query = String.Format("DELETE FROM Flight WHERE Number_Flight = {0}",
                       flight.Number_Flight);

            return querry.crudOperation(query);
        }

        public bool update(Flight flight)
        {
            string query = "";

            if(flight.Transit_Airfield != null)
            {
                query = String.Format("UPDATE Flight set Data = '{0}', Place_Dispatch = '{1}', Place_Arrival = '{2}', Transit_Airfield = '{3}', " +
                "Time_Dispatch = '{4}', Time_Dispatch_Transit = '{5}', Time_Arrival = '{6}', Time_Arrival_Transit = '{7}', Airplane = {8}" +
                "where Number_Flight = {9}",
                        flight.Date.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Place_Dispatch,
                        flight.Place_Arrival,
                        flight.Transit_Airfield,
                        flight.Time_Dispatch.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Time_Dispatch_Transit.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Time_Arrival.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Time_Arrival_Transit.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Airplane,
                        flight.Number_Flight);
            }
            else
            {
                query = String.Format("UPDATE Flight set Data = '{0}', Place_Dispatch = '{1}', Place_Arrival = '{2}', Transit_Airfield = '{3}', " +
                "Time_Dispatch = '{4}', Time_Dispatch_Transit = {5}, Time_Arrival = '{6}', Time_Arrival_Transit = {7}, Airplane = {8} " +
                "where Number_Flight = {9}",
                        flight.Date.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        flight.Place_Dispatch,
                        flight.Place_Arrival,
                        flight.Transit_Airfield,
                        flight.Time_Dispatch.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        SqlDateTime.Null,
                        flight.Time_Arrival.ToString("yyy-MM-dd HH:mm:ss.fff"),
                        SqlDateTime.Null,
                        flight.Airplane,
                        flight.Number_Flight);
            }

            return querry.crudOperation(query);
        }

        public List<FlightMinimize> GetMinimizes()
        {
            List<FlightMinimize> minimizes = new List<FlightMinimize>();

            foreach (Flight element in flights)
            {
                if (element.Transit_Airfield.Length > 1)
                    minimizes.Add(new FlightMinimize(element.Number_Flight, element.Place_Dispatch, element.Place_Arrival, element.Transit_Airfield));
                else
                    minimizes.Add(new FlightMinimize(element.Number_Flight, element.Place_Dispatch, element.Place_Arrival));
            }

            return minimizes;
        }
    }
}
