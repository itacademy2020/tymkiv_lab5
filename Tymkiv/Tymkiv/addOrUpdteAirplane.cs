﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tymkiv
{
    public partial class addOrUpdteAirplane : Form
    {
        public const string connectString = "Server=tcp:itacademy.database.windows.net,1433;" +
                                            "Database=Tymkiv;" +
                                            "User ID=Tymkiv;" +
                                            "Password=Hurd93522;" +
                                            "Trusted_Connection=False;Encrypt=True;";

        public Airplane airplane;
        public Airplane oldAirplane;
        public bool isReady;
        public addOrUpdteAirplane()
        {
            InitializeComponent();
            InitializeComboBox();
            this.airplane = new Airplane();
        }

        public addOrUpdteAirplane(Airplane airplane)
        {
            InitializeComponent();
            brandTextBox.Text = airplane.Brand;
            modelTextBox.Text = airplane.Model;
            flightRangeTextBox.Text = airplane.Flight_Range.ToString();
            capacityTextBox.Text = airplane.Capacity.ToString();
            tonnageTextBox.Text = airplane.Tonnage.ToString();
            fuelTextBox.Text = airplane.Fuel.ToString();
            InitializeComboBox(airplane.Type_Plane);
            oldAirplane = airplane;
        }

        private void InitializeComboBox(int id = 0)
        {
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                List<typePlane> types = new List<typePlane>();
                string query = "Select * from TypePlane";
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        typePlane type = new typePlane();
                        type.ID = (int)reader.GetValue(0);
                        type.type = reader.GetValue(1).ToString();

                        types.Add(type);
                    }
                }
                reader.Close();

                typeAirplaneComboBox.DataSource = types;
                typeAirplaneComboBox.DisplayMember = "type";
                if(id>0)
                    typeAirplaneComboBox.SelectedItem = types.Where(c => c.ID == id).FirstOrDefault();
            }
        }

        private void intTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back) return;
            else
                e.Handled = true;
        }

        private void floatTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void stringTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)e.KeyChar == (Char)Keys.Back) return;
            if (char.IsDigit(e.KeyChar) || char.IsLetter(e.KeyChar)) return;
            e.Handled = true;
        }

        private void accept_Click(object sender, EventArgs e)
        {
            airplane = new Airplane();
            try
            {
                if (typeAirplaneComboBox.SelectedIndex < 0)
                    throw new ArgumentException("Не вибрано елемент в випадаючому списку");
                else
                {
                    typePlane type = (typePlane)(typeAirplaneComboBox.SelectedItem);
                    airplane.Type_Plane = type.ID;
                }
                     

                if (modelTextBox.Text.Length > 0)
                    airplane.Model = modelTextBox.Text.ToString();
                else
                    throw new ArgumentException("Введіть модель літака");

                if (brandTextBox.Text.Length > 0)
                    airplane.Brand = brandTextBox.Text.ToString();
                else
                    throw new ArgumentException("Введіть бренд літака");

                if (flightRangeTextBox.Text.Length > 0)
                    airplane.Flight_Range = float.Parse(flightRangeTextBox.Text.ToString());
                else
                    throw new ArgumentException("Введіть дальність польоту літака");

                if (capacityTextBox.Text.Length > 0)
                    airplane.Capacity = int.Parse(capacityTextBox.Text.ToString());
                else
                    throw new ArgumentException("Введіть пасажиромісткість літака");

                if (tonnageTextBox.Text.Length > 0)
                    airplane.Tonnage = float.Parse(tonnageTextBox.Text.ToString());
                else
                    throw new ArgumentException("Введіть тоннаж літака");

                if (fuelTextBox.Text.Length > 0)
                    airplane.Fuel = float.Parse(fuelTextBox.Text.ToString());
                else
                    throw new ArgumentException("Введіть місткість баку літака");

                if(oldAirplane!=null)
                {
                    airplane.Serial_Number = oldAirplane.Serial_Number;
                }
                isReady = true;
                this.Close();
            }
            catch(ArgumentException aex)
            {
                MessageBox.Show(aex.Message);
            }
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
