﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tymkiv
{
    class FlightMinimize
    {
        public string FullFlight { get; set; }

        public int ID { get; set; }


        public FlightMinimize(int id, string dispatch, string arrive, string transit = "")
        {
            if(transit.Length > 1)
            {
                FullFlight = $"{id}: {dispatch} - {transit} - {arrive}";
            }
            else
                FullFlight = $"{id}: {dispatch}  - {arrive}";

            this.ID = id;
        }
    }
}
