﻿namespace Tymkiv
{
    partial class addOrUpdateEmploee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.fullNameTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.addressTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.brthdayDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.dateEmploymentDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.accept = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Повне ім\'я";
            // 
            // fullNameTextBox
            // 
            this.fullNameTextBox.Location = new System.Drawing.Point(13, 37);
            this.fullNameTextBox.Name = "fullNameTextBox";
            this.fullNameTextBox.Size = new System.Drawing.Size(179, 27);
            this.fullNameTextBox.TabIndex = 1;
            this.fullNameTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stringTextBox_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Адреса";
            // 
            // addressTextBox
            // 
            this.addressTextBox.Location = new System.Drawing.Point(13, 94);
            this.addressTextBox.Name = "addressTextBox";
            this.addressTextBox.Size = new System.Drawing.Size(179, 27);
            this.addressTextBox.TabIndex = 1;
            this.addressTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stringAndNumberTextBox_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(208, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(136, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "День народження";
            // 
            // brthdayDateTimePicker
            // 
            this.brthdayDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.brthdayDateTimePicker.Location = new System.Drawing.Point(208, 37);
            this.brthdayDateTimePicker.Name = "brthdayDateTimePicker";
            this.brthdayDateTimePicker.Size = new System.Drawing.Size(184, 27);
            this.brthdayDateTimePicker.TabIndex = 3;
            // 
            // dateEmploymentDateTimePicker
            // 
            this.dateEmploymentDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateEmploymentDateTimePicker.Location = new System.Drawing.Point(208, 92);
            this.dateEmploymentDateTimePicker.Name = "dateEmploymentDateTimePicker";
            this.dateEmploymentDateTimePicker.Size = new System.Drawing.Size(184, 27);
            this.dateEmploymentDateTimePicker.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(208, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(179, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Дата працевлаштування";
            // 
            // accept
            // 
            this.accept.Location = new System.Drawing.Point(13, 144);
            this.accept.Name = "accept";
            this.accept.Size = new System.Drawing.Size(379, 41);
            this.accept.TabIndex = 4;
            this.accept.Text = "Підтвердити";
            this.accept.UseVisualStyleBackColor = true;
            this.accept.Click += new System.EventHandler(this.accept_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(13, 191);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(379, 41);
            this.cancel.TabIndex = 4;
            this.cancel.Text = "Відмінити";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // addOrUpdateEmploee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(403, 241);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.accept);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.dateEmploymentDateTimePicker);
            this.Controls.Add(this.brthdayDateTimePicker);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.addressTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.fullNameTextBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "addOrUpdateEmploee";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "addOrUpdateEmploee";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox fullNameTextBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox addressTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker brthdayDateTimePicker;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dateEmploymentDateTimePicker;
        private System.Windows.Forms.Button accept;
        private System.Windows.Forms.Button cancel;
    }
}