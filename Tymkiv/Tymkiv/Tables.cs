﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Tymkiv
{
    public partial class Tables : Form
    {


        private Context context;
        public Tables()
        {
            InitializeComponent();
            context = new Context();
            tabControl1.SelectedIndex = 0;
            printDataAirplane();
        }
        private void Clear(DataGridView view)
        {
            int rowsCount = view.Rows.Count;
            for (int i = 0; i < rowsCount; i++)
            {
                view.Rows.Remove(view.Rows[0]);
            }
        }

        public void printDataAirplane()
        {
            Clear(this.AirplaneGrid);
            context.Airplanes.wtriteData();
            AirplaneGrid.DataSource = new BindingList<Airplane>(context.Airplanes.AllAirplanes);
        }

        public void printDataEmploee()
        {
            Clear(this.emploeeGrid);
            context.Emploees.wtriteData();
            emploeeGrid.DataSource = new BindingList<Emploee>(context.Emploees.AllEmploee);
        }

        public void printDataFlight()
        {
            Clear(this.flightGrid);
            bool i = context.Flights.wtriteData();
            flightGrid.DataSource = new BindingList<Flight>(context.Flights.AllFlights);
        }

        public void printDataPassenger()
        {
            Clear(this.passengerGrid);
            context.Passengers.wtriteData();
            passengerGrid.DataSource = new BindingList<Passenger>(context.Passengers.AllPassangers);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int tab = tabControl1.SelectedIndex;
            if (tab == 0)
            {
                printDataAirplane();
            }
            if(tab == 1)
            {
                printDataFlight();
            }
            if(tab == 2)
            {
                printDataEmploee();
            }
            if(tab == 3)
            {
                printDataPassenger();
            }

        }

        private void addAirplate_Click(object sender, EventArgs e)
        {
            addOrUpdteAirplane addOrUpdteAirplane = new addOrUpdteAirplane();
            addOrUpdteAirplane.ShowDialog();

            if(addOrUpdteAirplane.isReady)
            {
                if(context.Airplanes.add(addOrUpdteAirplane.airplane))
                {
                    MessageBox.Show("Запис додано успішно");
                    printDataAirplane();
                }
                else
                    MessageBox.Show("Запис не додано");
            }   
        }

        private void deleteAirplate_Click(object sender, EventArgs e)
        {
            Airplane selectedItem = AirplaneGrid.Rows[AirplaneGrid.CurrentCell.RowIndex].DataBoundItem as Airplane;

            if (context.Airplanes.delete(selectedItem))
            {
                MessageBox.Show("Запис успішно видалено");
                printDataAirplane();
            }
            else
                MessageBox.Show("Запис не видалено");


        }

        private void updateAirplate_Click(object sender, EventArgs e)
        {
            Airplane selectedItem = AirplaneGrid.Rows[AirplaneGrid.CurrentCell.RowIndex].DataBoundItem as Airplane;

            addOrUpdteAirplane addOrUpdteAirplane = new addOrUpdteAirplane(selectedItem);
            addOrUpdteAirplane.ShowDialog();

            if(addOrUpdteAirplane.isReady)
            {
                if (context.Airplanes.update(addOrUpdteAirplane.airplane))
                {
                    MessageBox.Show("Запис успішно редаговано");
                    printDataAirplane();
                }
                else
                    MessageBox.Show("Запис не редаговано");
            }
        }

        private void addFlight_Click(object sender, EventArgs e)
        {
            addOrUpdateFlight add = new addOrUpdateFlight();

            add.ShowDialog();

            if (add.isReady)
            {
                if (context.Flights.add(add.flight))
                {
                    MessageBox.Show("Запис додано успішно");
                    printDataFlight();
                }
                else
                    MessageBox.Show("Запис не додано");
            }
        }

        private void deleteFlight_Click(object sender, EventArgs e)
        {
            Flight selectedItem = flightGrid.Rows[flightGrid.CurrentCell.RowIndex].DataBoundItem as Flight;

            if (context.Flights.delete(selectedItem))
            {
                MessageBox.Show("Запис успішно видалено");
                printDataFlight();
            }
            else
                MessageBox.Show("Запис не видалено");


        }

        private void updateFlight_Click(object sender, EventArgs e)
        {

            Flight selectedItem = flightGrid.Rows[flightGrid.CurrentCell.RowIndex].DataBoundItem as Flight;
            addOrUpdateFlight add = new addOrUpdateFlight(selectedItem);

            add.ShowDialog();

            if (add.isReady)
            {
                if (context.Flights.update(add.flight))
                {
                    MessageBox.Show("Запис редаговано успішно");
                    printDataFlight();
                }
                else
                    MessageBox.Show("Запис не редаговано");
            }
        }

        private void addEmploee_Click(object sender, EventArgs e)
        {
            addOrUpdateEmploee add = new addOrUpdateEmploee();

            add.ShowDialog();

            if (add.isReady)
            {
                if (context.Emploees.add(add.emploee))
                {
                    MessageBox.Show("Запис додано успішно");
                    printDataEmploee();
                }
                else
                    MessageBox.Show("Запис не додано");
            }
        }

        private void updateEmploee_Click(object sender, EventArgs e)
        {
            Emploee selectedItem = emploeeGrid.Rows[emploeeGrid.CurrentCell.RowIndex].DataBoundItem as Emploee;
            addOrUpdateEmploee add = new addOrUpdateEmploee(selectedItem);

            add.ShowDialog();

            if (add.isReady)
            {
                if (context.Emploees.update(add.emploee))
                {
                    MessageBox.Show("Запис редаговано успішно");
                    printDataEmploee();
                }
                else
                    MessageBox.Show("Запис не редаговано");
            }
        }

        private void deleteEmploee_Click(object sender, EventArgs e)
        {
            Emploee selectedItem = emploeeGrid.Rows[emploeeGrid.CurrentCell.RowIndex].DataBoundItem as Emploee;

            if (context.Emploees.delete(selectedItem))
            {
                MessageBox.Show("Запис успішно видалено");
                printDataEmploee();
            }
            else
                MessageBox.Show("Запис не видалено");
        }

        private void addPassenger_Click(object sender, EventArgs e)
        {
            addOrUpdatePassenger add = new addOrUpdatePassenger();

            add.ShowDialog();

            if (add.isReady)
            {
                if (context.Passengers.add(add.passenger))
                {
                    MessageBox.Show("Запис додано успішно");
                    printDataPassenger();
                }
                else
                    MessageBox.Show("Запис не додано");
            }
        }

        private void updatePassenger_Click(object sender, EventArgs e)
        {
            Passenger selectedItem = passengerGrid.Rows[passengerGrid.CurrentCell.RowIndex].DataBoundItem as Passenger;
            addOrUpdatePassenger add = new addOrUpdatePassenger(selectedItem);

            add.ShowDialog();

            if (add.isReady)
            {
                if (context.Passengers.update(add.passenger))
                {
                    MessageBox.Show("Запис редаговано успішно");
                    printDataPassenger();
                }
                else
                    MessageBox.Show("Запис не редаговано");
            }
        }

        private void deletePassenger_Click(object sender, EventArgs e)
        {
            Passenger selectedItem = passengerGrid.Rows[passengerGrid.CurrentCell.RowIndex].DataBoundItem as Passenger;

            if (context.Passengers.delete(selectedItem))
            {
                MessageBox.Show("Запис успішно видалено");
                printDataPassenger();
            }
            else
                MessageBox.Show("Запис не видалено");
        }
    }
}
