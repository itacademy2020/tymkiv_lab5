﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tymkiv
{
    public partial class addOrUpdateFlight : Form
    {
        public const string connectString = "Server=tcp:itacademy.database.windows.net,1433;" +
                                            "Database=Tymkiv;" +
                                            "User ID=Tymkiv;" +
                                            "Password=Hurd93522;" +
                                            "Trusted_Connection=False;Encrypt=True;";
        public Flight flight;
        private Flight oldFlight;
        public bool isReady;
        public addOrUpdateFlight()
        {
            InitializeComponent();
            InitializeComboBox();
            InitializeDataPicker();
        }

        public addOrUpdateFlight(Flight flight)
        {
            InitializeComponent();
            InitializeComboBox(flight.Airplane);
            InitializeDataPicker();

            dateDataTime.Value = flight.Date;
            placeDispatchTextBox.Text = flight.Place_Dispatch;
            arrivalTextBox.Text = flight.Place_Arrival;
            if(flight.Transit_Airfield != null)
                transitTextBox.Text = flight.Transit_Airfield;

            timeArriveDateTimePicker.Value = flight.Time_Arrival;
            if (flight.Time_Arrival_Transit != DateTime.Parse("01.01.0001 0:00:00"))
                timeArriveTransitDateTimePicker.Value = flight.Time_Arrival_Transit;

            timeDispatchDateTimePicker.Value = flight.Time_Dispatch;
            if (flight.Time_Dispatch_Transit != DateTime.Parse("01.01.0001 0:00:00"))
                timeDispatchTransitDateTimePicker.Value = flight.Time_Dispatch_Transit;

            oldFlight = flight;
        }

        private void InitializeComboBox(int id = 0)
        {
            Airplanes airplanes = new Airplanes();
            airplanes.wtriteData();
            List<Airplane> elements = new List<Airplane>(airplanes.AllAirplanes);
            airplaneComboBox.DataSource = elements;
            airplaneComboBox.DisplayMember = "FuelName";
            if (id > 0)
                airplaneComboBox.SelectedItem = elements.Where(c => c.Serial_Number == id).FirstOrDefault();
            else
                airplaneComboBox.SelectedIndex = -1;
        }

        public void InitializeDataPicker()
        {
            dateDataTime.Value = DateTime.Now;
        }

        private void accept_Click(object sender, EventArgs e)
        {
            flight = new Flight();
            try
            {
                if (airplaneComboBox.SelectedIndex < 0)
                    throw new ArgumentException("Не вибрано елемент в випадаючому списку");
                else
                {
                    Airplane airplane = (Airplane)(airplaneComboBox.SelectedItem);
                    flight.Airplane = airplane.Serial_Number;
                }

                if (placeDispatchTextBox.Text.Length > 0)
                    flight.Place_Dispatch = placeDispatchTextBox.Text.ToString();
                else
                    throw new ArgumentException("Введіть місце вильоту");

                if (arrivalTextBox.Text.Length > 0)
                    flight.Place_Arrival = arrivalTextBox.Text.ToString();
                else
                    throw new ArgumentException("Введіть місце прибуття");

                if (dateDataTime.Value != dateDataTime.MinDate)
                    flight.Date = dateDataTime.Value;
                else
                    throw new ArgumentException("Введіть дату вильоту рейса");

                if (timeDispatchDateTimePicker.Value >= flight.Date)
                    flight.Time_Dispatch = timeDispatchDateTimePicker.Value;
                else
                    throw new ArgumentException("Час вильоту введений не коректно");

                if (transitTextBox.Text.Length > 1)
                {
                    flight.Transit_Airfield = transitTextBox.Text.ToString();

                    if (timeDispatchTransitDateTimePicker.Value > flight.Time_Dispatch)
                        flight.Time_Dispatch_Transit = timeDispatchTransitDateTimePicker.Value;
                    else
                        throw new ArgumentException("Час посадки у транзитне місто введений не коректно");

                    if (timeArriveTransitDateTimePicker.Value > flight.Time_Dispatch_Transit)
                        flight.Time_Arrival_Transit = timeArriveTransitDateTimePicker.Value;
                    else
                        throw new ArgumentException("Час вильоту з транзитного міста введений не коректно");
                }
                else
                {
                    flight.Transit_Airfield = null;
                }

                if (flight.Transit_Airfield != null)
                {
                    if (timeArriveDateTimePicker.Value > flight.Time_Arrival_Transit)
                        flight.Time_Arrival = timeArriveDateTimePicker.Value;
                    else
                        throw new ArgumentException("Час прибуття введений не коректно");
                }
                else
                {
                    if (timeArriveDateTimePicker.Value > flight.Time_Dispatch)
                        flight.Time_Arrival = timeArriveDateTimePicker.Value;
                    else
                        throw new ArgumentException("Час прибуття введений не коректно");
                }

                if (oldFlight != null)
                {
                    flight.Number_Flight = oldFlight.Number_Flight;
                }
                isReady = true;
                this.Close();
            }
            catch (ArgumentException aex)
            {
                MessageBox.Show(aex.Message);
            }
        }

        private void stringTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)e.KeyChar == (Char)Keys.Back) return;
            if (char.IsLetter(e.KeyChar) || (char)e.KeyChar == '-' || (char)e.KeyChar == ' ' || (char)e.KeyChar == '`') return;
            e.Handled = true;
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void timeDispatchTransitDateTimePicker_ValueChanged(object sender, EventArgs e)
        {
            setFormatDateTimePicker(sender as DateTimePicker);
        }

        private void transitTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if(transitTextBox.Text.Length > 0)
            {
                setFormatDateTimePicker(timeDispatchTransitDateTimePicker);
                setFormatDateTimePicker(timeArriveTransitDateTimePicker);
                timeDispatchTransitDateTimePicker.Value = timeDispatchDateTimePicker.Value;
                timeArriveTransitDateTimePicker.Value = timeDispatchDateTimePicker.Value;
            }
        }

        private void setFormatDateTimePicker(DateTimePicker dtp)
        {
            if (!dtp.ShowCheckBox)
                dtp.CustomFormat = "dd-MM-yyyy HH:mm";
            else
                dtp.CustomFormat = "''";
        }
    }
}
