﻿namespace Tymkiv
{
    partial class addOrUpdateFlight
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dateDataTime = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.placeDispatchTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.transitTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.arrivalTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.airplaneComboBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.timeDispatchDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.timeDispatchTransitDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.timeArriveDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.timeArriveTransitDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label9 = new System.Windows.Forms.Label();
            this.accept = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Дата вильоту";
            // 
            // dateDataTime
            // 
            this.dateDataTime.Location = new System.Drawing.Point(13, 37);
            this.dateDataTime.Name = "dateDataTime";
            this.dateDataTime.Size = new System.Drawing.Size(205, 27);
            this.dateDataTime.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(109, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Місце вильоту";
            // 
            // placeDispatchTextBox
            // 
            this.placeDispatchTextBox.Location = new System.Drawing.Point(13, 95);
            this.placeDispatchTextBox.Name = "placeDispatchTextBox";
            this.placeDispatchTextBox.Size = new System.Drawing.Size(205, 27);
            this.placeDispatchTextBox.TabIndex = 3;
            this.placeDispatchTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stringTextBox_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 129);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Транзитне місто";
            // 
            // transitTextBox
            // 
            this.transitTextBox.Location = new System.Drawing.Point(13, 152);
            this.transitTextBox.Name = "transitTextBox";
            this.transitTextBox.Size = new System.Drawing.Size(205, 27);
            this.transitTextBox.TabIndex = 3;
            this.transitTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stringTextBox_KeyPress);
            this.transitTextBox.KeyUp += new System.Windows.Forms.KeyEventHandler(this.transitTextBox_KeyUp);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 191);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 20);
            this.label4.TabIndex = 4;
            this.label4.Text = "Місце прибуття";
            // 
            // arrivalTextBox
            // 
            this.arrivalTextBox.Location = new System.Drawing.Point(13, 214);
            this.arrivalTextBox.Name = "arrivalTextBox";
            this.arrivalTextBox.Size = new System.Drawing.Size(205, 27);
            this.arrivalTextBox.TabIndex = 3;
            this.arrivalTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stringTextBox_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 248);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 20);
            this.label5.TabIndex = 5;
            this.label5.Text = "Літак";
            // 
            // airplaneComboBox
            // 
            this.airplaneComboBox.FormattingEnabled = true;
            this.airplaneComboBox.Location = new System.Drawing.Point(13, 272);
            this.airplaneComboBox.Name = "airplaneComboBox";
            this.airplaneComboBox.Size = new System.Drawing.Size(205, 28);
            this.airplaneComboBox.TabIndex = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(241, 69);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(245, 20);
            this.label6.TabIndex = 7;
            this.label6.Text = "Час вильоту з початкового пункту";
            // 
            // timeDispatchDateTimePicker
            // 
            this.timeDispatchDateTimePicker.CustomFormat = "dd-MM-yyyy HH:mm ";
            this.timeDispatchDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeDispatchDateTimePicker.Location = new System.Drawing.Point(241, 95);
            this.timeDispatchDateTimePicker.Name = "timeDispatchDateTimePicker";
            this.timeDispatchDateTimePicker.Size = new System.Drawing.Size(213, 27);
            this.timeDispatchDateTimePicker.TabIndex = 8;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(241, 129);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(229, 20);
            this.label7.TabIndex = 7;
            this.label7.Text = "Час прибуття в транзитне місто";
            // 
            // timeDispatchTransitDateTimePicker
            // 
            this.timeDispatchTransitDateTimePicker.CustomFormat = " ";
            this.timeDispatchTransitDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeDispatchTransitDateTimePicker.Location = new System.Drawing.Point(241, 152);
            this.timeDispatchTransitDateTimePicker.Name = "timeDispatchTransitDateTimePicker";
            this.timeDispatchTransitDateTimePicker.Size = new System.Drawing.Size(213, 27);
            this.timeDispatchTransitDateTimePicker.TabIndex = 8;
            this.timeDispatchTransitDateTimePicker.ValueChanged += new System.EventHandler(this.timeDispatchTransitDateTimePicker_ValueChanged);
            // 
            // timeArriveDateTimePicker
            // 
            this.timeArriveDateTimePicker.CustomFormat = "dd-MM-yyyy HH:mm ";
            this.timeArriveDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeArriveDateTimePicker.Location = new System.Drawing.Point(241, 271);
            this.timeArriveDateTimePicker.Name = "timeArriveDateTimePicker";
            this.timeArriveDateTimePicker.Size = new System.Drawing.Size(213, 27);
            this.timeArriveDateTimePicker.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(241, 248);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(222, 20);
            this.label8.TabIndex = 7;
            this.label8.Text = "Час прибуття в кінцевий пункт";
            // 
            // timeArriveTransitDateTimePicker
            // 
            this.timeArriveTransitDateTimePicker.CustomFormat = " ";
            this.timeArriveTransitDateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.timeArriveTransitDateTimePicker.Location = new System.Drawing.Point(241, 214);
            this.timeArriveTransitDateTimePicker.Name = "timeArriveTransitDateTimePicker";
            this.timeArriveTransitDateTimePicker.Size = new System.Drawing.Size(213, 27);
            this.timeArriveTransitDateTimePicker.TabIndex = 8;
            this.timeArriveTransitDateTimePicker.ValueChanged += new System.EventHandler(this.timeDispatchTransitDateTimePicker_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(241, 188);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(226, 20);
            this.label9.TabIndex = 7;
            this.label9.Text = "Час вильоту з тразитного міста";
            // 
            // accept
            // 
            this.accept.Location = new System.Drawing.Point(13, 322);
            this.accept.Name = "accept";
            this.accept.Size = new System.Drawing.Size(473, 45);
            this.accept.TabIndex = 9;
            this.accept.Text = "Підтвердити";
            this.accept.UseVisualStyleBackColor = true;
            this.accept.Click += new System.EventHandler(this.accept_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(13, 373);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(473, 45);
            this.cancel.TabIndex = 9;
            this.cancel.Text = "Відмінити";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // addOrUpdateFlight
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(496, 428);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.accept);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.timeArriveTransitDateTimePicker);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.timeArriveDateTimePicker);
            this.Controls.Add(this.timeDispatchTransitDateTimePicker);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.timeDispatchDateTimePicker);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.airplaneComboBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.arrivalTextBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.transitTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.placeDispatchTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateDataTime);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "addOrUpdateFlight";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "addOrUpdateFlight";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dateTimePicker;
        private System.Windows.Forms.DateTimePicker dateDataTime;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox placeDispatchTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox transitTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox arrivalTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox airplaneComboBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.DateTimePicker timeDispatchDateTimePicker;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DateTimePicker timeDispatchTransitDateTimePicker;
        private System.Windows.Forms.DateTimePicker timeArriveDateTimePicker;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DateTimePicker timeArriveTransitDateTimePicker;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button accept;
        private System.Windows.Forms.Button cancel;
    }
}