﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;

namespace Tymkiv
{
    class Emploees
    {
        private List<Emploee> emploees;
        private QuerryToDatabase querry;
        public const string connectString = "Server=tcp:itacademy.database.windows.net,1433;" +
                                            "Database=Tymkiv;" +
                                            "User ID=Tymkiv;" +
                                            "Password=Hurd93522;" +
                                            "Trusted_Connection=False;Encrypt=True;";

        public List<Emploee> AllEmploee
        {
            private set { }
            get
            {
                return emploees;
            }
        }

        public Emploees()
        {
            emploees = new List<Emploee>();
            querry = new QuerryToDatabase();
        }

        public bool wtriteData()
        {
            emploees = new List<Emploee>();
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                try
                {
                    string query = "Select * from Emploee";
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            Emploee emploee = new Emploee();
                            emploee.Number_Emploee = (int)reader.GetValue(0);
                            emploee.Full_Name = reader.GetValue(1).ToString();
                            emploee.brthday = (DateTime)reader.GetValue(2);
                            emploee.addres = reader.GetValue(3).ToString();
                            emploee.Date_Employment = (DateTime)reader.GetValue(4);
                            
                            emploees.Add(emploee);
                        }
                    }
                    reader.Close();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public bool add(Emploee emploee)
        {
            string query = String.Format("INSERT INTO Emploee " +
                        "VALUES ('{0}', '{1}', '{2}', '{3}')",
                        emploee.Full_Name,
                        emploee.brthday.ToString("yyy-MM-dd"),
                        emploee.addres,
                        emploee.Date_Employment.ToString("yyy-MM-dd")
                        );

            return querry.crudOperation(query);
        }

        public bool update(Emploee emploee)
        {
            string query = String.Format("UPDATE Emploee set Full_Name = '{0}', Brthday = '{1}', Address = '{2}', Date_Employment = '{3}' " +
                "where Number_Emploee = {4}",
                emploee.Full_Name,
                emploee.brthday.ToString("yyy-MM-dd"),
                emploee.addres,
                emploee.Date_Employment.ToString("yyy-MM-dd"),
                emploee.Number_Emploee);

            return querry.crudOperation(query);
        }

        public bool delete(Emploee emploee)
        {
            string query = String.Format("DELETE FROM Emploee WHERE Number_Emploee = {0}",
                emploee.Number_Emploee);

            return querry.crudOperation(query);
        }
    }
}
