﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Tymkiv
{
    public partial class addOrUpdatePassenger : Form
    {
        public Passenger passenger;
        public bool isReady;
        private Passenger oldPassenger;

        public const string connectString = "Server=tcp:itacademy.database.windows.net,1433;" +
                                            "Database=Tymkiv;" +
                                            "User ID=Tymkiv;" +
                                            "Password=Hurd93522;" +
                                            "Trusted_Connection=False;Encrypt=True;";
        public addOrUpdatePassenger()
        {
            InitializeComponent();
            InitializeComboBox();
        }
        public addOrUpdatePassenger(Passenger passenger)
        {
            InitializeComponent();
            InitializeComboBox(passenger.Type_Document);

            nameTextBox.Text = passenger.name;
            surnameTextBox.Text = passenger.surname;
            phoneTextBox.Text = passenger.phone;
            addressTextBox.Text = passenger.address;
            mailTextBox.Text = passenger.email;

            oldPassenger = passenger;
        }

        private void InitializeComboBox(int id = 0)
        {
            using (SqlConnection connection = new SqlConnection(connectString))
            {
                List<typeDocument> types = new List<typeDocument>();
                string query = "Select * from TypeDocument";
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                SqlDataReader reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        typeDocument type = new typeDocument();
                        type.id = (int)reader.GetValue(0);
                        type.type = reader.GetValue(1).ToString();

                        types.Add(type);
                    }
                }
                reader.Close();

                typeDocumentComboBox.DataSource = types;
                typeDocumentComboBox.DisplayMember = "type";
                if (id > 0)
                    typeDocumentComboBox.SelectedItem = types.Where(c => c.id == id).FirstOrDefault();
            }
        }

        bool isValidEmail(string email)
        {
            string pattern = "[.\\-_a-z0-9]+@([a-z0-9][\\-a-z0-9]+\\.)+[a-z]{2,6}";
            Match isMatch = Regex.Match(email, pattern, RegexOptions.IgnoreCase);
            return isMatch.Success;
        }

        private void accept_Click(object sender, EventArgs e)
        {
            passenger = new Passenger();
            try
            {
                if (typeDocumentComboBox.SelectedIndex < 0)
                    throw new ArgumentException("Не вибрано елемент в випадаючому списку");
                else
                {
                    typeDocument type = (typeDocument)(typeDocumentComboBox.SelectedItem);
                    passenger.Type_Document = type.id;
                }

                if (nameTextBox.Text.Length > 0)
                    passenger.name = nameTextBox.Text.ToString();
                else
                    throw new ArgumentException("Введіть ім'я пасажира");

                if (surnameTextBox.Text.Length > 0)
                    passenger.surname = surnameTextBox.Text.ToString();
                else
                    throw new ArgumentException("Введіть прізвище пасажира");

                if (addressTextBox.Text.Length > 0)
                    passenger.address = addressTextBox.Text;
                else
                    throw new ArgumentException("Введіть адрес");

                phoneTextBox.Mask = "00000000000";
                string phone = Convert.ToString(phoneTextBox.Text);
                phoneTextBox.Mask = "(999) 000-0000";

                if (phone.Length == 10)
                    passenger.phone = phone;
                else
                    throw new ArgumentException("Введіть телефон");

                if (isValidEmail(mailTextBox.Text))
                    passenger.email= mailTextBox.Text;
                else
                    throw new ArgumentException("Введіть корректну електронну пошту");

                if (oldPassenger != null)
                {
                    passenger.Number_Document = oldPassenger.Number_Document;
                }
                isReady = true;
                this.Close();
            }
            catch (ArgumentException aex)
            {
                MessageBox.Show(aex.Message);
            }
        }

        private void cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void stringTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)e.KeyChar == (Char)Keys.Back) return;
            if (char.IsLetter(e.KeyChar) || (char)e.KeyChar == '-' || (char)e.KeyChar == ' ' || (char)e.KeyChar == '`') return;
            e.Handled = true;
        }

        private void mailTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= 'A' && e.KeyChar <= 'Z') || (e.KeyChar >= 'a' && e.KeyChar <= 'z') || 
                (e.KeyChar >= '0' && e.KeyChar <= '9') || e.KeyChar == '@' || e.KeyChar == '.' || e.KeyChar == '_' || e.KeyChar == (char)Keys.Back
                || e.KeyChar == '!' || e.KeyChar == '#' || e.KeyChar == '$' || e.KeyChar == '%' || e.KeyChar == '&' || e.KeyChar == '*'
                || e.KeyChar == '+' || e.KeyChar == '-' || e.KeyChar == '/' || e.KeyChar == '=' || e.KeyChar == '?' || e.KeyChar == '^'
                || e.KeyChar == '`' || e.KeyChar == '{' || e.KeyChar == '|' || e.KeyChar == '}' || e.KeyChar == '~')
                return;
            else 
                e.Handled = true;
        }

        private void phoneTextBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((char)e.KeyChar == (Char)Keys.Back) return;
            if (char.IsDigit(e.KeyChar)) return;
            e.Handled = true;
        }

        
    }
}
