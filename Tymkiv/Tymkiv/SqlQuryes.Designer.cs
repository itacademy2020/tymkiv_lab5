﻿namespace Tymkiv
{
    partial class SqlQuryes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.query1 = new System.Windows.Forms.Button();
            this.crawFlightGrid = new System.Windows.Forms.DataGridView();
            this.flightComboBox = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.rangeTextBox = new System.Windows.Forms.TextBox();
            this.airplaneGrid = new System.Windows.Forms.DataGridView();
            this.query2 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.query3 = new System.Windows.Forms.Button();
            this.cityComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.passengerGrid = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.crawFlightGrid)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.airplaneGrid)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passengerGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(13, 13);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1250, 527);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.query1);
            this.tabPage1.Controls.Add(this.crawFlightGrid);
            this.tabPage1.Controls.Add(this.flightComboBox);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1242, 494);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Вивід екіпажів вказаних рейсів";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // query1
            // 
            this.query1.Location = new System.Drawing.Point(1049, 26);
            this.query1.Name = "query1";
            this.query1.Size = new System.Drawing.Size(179, 44);
            this.query1.TabIndex = 3;
            this.query1.Text = "Вивести";
            this.query1.UseVisualStyleBackColor = true;
            this.query1.Click += new System.EventHandler(this.query1_Click);
            // 
            // crawFlightGrid
            // 
            this.crawFlightGrid.AllowUserToAddRows = false;
            this.crawFlightGrid.AllowUserToDeleteRows = false;
            this.crawFlightGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.crawFlightGrid.Location = new System.Drawing.Point(4, 76);
            this.crawFlightGrid.Name = "crawFlightGrid";
            this.crawFlightGrid.ReadOnly = true;
            this.crawFlightGrid.RowHeadersWidth = 51;
            this.crawFlightGrid.Size = new System.Drawing.Size(1235, 415);
            this.crawFlightGrid.TabIndex = 2;
            this.crawFlightGrid.Text = "dataGridView1";
            // 
            // flightComboBox
            // 
            this.flightComboBox.FormattingEnabled = true;
            this.flightComboBox.Location = new System.Drawing.Point(4, 28);
            this.flightComboBox.Name = "flightComboBox";
            this.flightComboBox.Size = new System.Drawing.Size(290, 28);
            this.flightComboBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Рейс";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.rangeTextBox);
            this.tabPage2.Controls.Add(this.airplaneGrid);
            this.tabPage2.Controls.Add(this.query2);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Location = new System.Drawing.Point(4, 29);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1242, 494);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Вивід літаків у яких дальність польоту більша за вказану";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // rangeTextBox
            // 
            this.rangeTextBox.Location = new System.Drawing.Point(6, 32);
            this.rangeTextBox.Name = "rangeTextBox";
            this.rangeTextBox.Size = new System.Drawing.Size(208, 27);
            this.rangeTextBox.TabIndex = 4;
            this.rangeTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.intTextBox_KeyPress);
            // 
            // airplaneGrid
            // 
            this.airplaneGrid.AllowUserToAddRows = false;
            this.airplaneGrid.AllowUserToDeleteRows = false;
            this.airplaneGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.airplaneGrid.Location = new System.Drawing.Point(4, 76);
            this.airplaneGrid.Name = "airplaneGrid";
            this.airplaneGrid.ReadOnly = true;
            this.airplaneGrid.RowHeadersWidth = 51;
            this.airplaneGrid.Size = new System.Drawing.Size(1235, 415);
            this.airplaneGrid.TabIndex = 2;
            this.airplaneGrid.Text = "dataGridView1";
            // 
            // query2
            // 
            this.query2.Location = new System.Drawing.Point(1051, 30);
            this.query2.Name = "query2";
            this.query2.Size = new System.Drawing.Size(179, 44);
            this.query2.TabIndex = 3;
            this.query2.Text = "Вивести";
            this.query2.UseVisualStyleBackColor = true;
            this.query2.Click += new System.EventHandler(this.query2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(137, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Дальність польоту";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.query3);
            this.tabPage3.Controls.Add(this.cityComboBox);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.passengerGrid);
            this.tabPage3.Location = new System.Drawing.Point(4, 29);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1242, 494);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Клієнти які купили квиток у вказане місто";
            // 
            // query3
            // 
            this.query3.Location = new System.Drawing.Point(1060, 28);
            this.query3.Name = "query3";
            this.query3.Size = new System.Drawing.Size(179, 44);
            this.query3.TabIndex = 3;
            this.query3.Text = "Вивести";
            this.query3.UseVisualStyleBackColor = true;
            this.query3.Click += new System.EventHandler(this.query3_Click);
            // 
            // cityComboBox
            // 
            this.cityComboBox.FormattingEnabled = true;
            this.cityComboBox.Location = new System.Drawing.Point(7, 28);
            this.cityComboBox.Name = "cityComboBox";
            this.cityComboBox.Size = new System.Drawing.Size(290, 28);
            this.cityComboBox.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Місто";
            // 
            // passengerGrid
            // 
            this.passengerGrid.AllowUserToAddRows = false;
            this.passengerGrid.AllowUserToDeleteRows = false;
            this.passengerGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.passengerGrid.Location = new System.Drawing.Point(7, 76);
            this.passengerGrid.Name = "passengerGrid";
            this.passengerGrid.ReadOnly = true;
            this.passengerGrid.RowHeadersWidth = 51;
            this.passengerGrid.Size = new System.Drawing.Size(1235, 415);
            this.passengerGrid.TabIndex = 2;
            this.passengerGrid.Text = "dataGridView1";
            // 
            // SqlQuryes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1275, 552);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SqlQuryes";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SqlQuryes";
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.crawFlightGrid)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.airplaneGrid)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.passengerGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button query1;
        private System.Windows.Forms.DataGridView crawFlightGrid;
        private System.Windows.Forms.ComboBox flightComboBox;
        private System.Windows.Forms.Button query2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView airplaneGrid;
        private System.Windows.Forms.TextBox rangeTextBox;
        private System.Windows.Forms.DataGridView passengerGrid;
        private System.Windows.Forms.ComboBox cityComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button query3;
    }
}