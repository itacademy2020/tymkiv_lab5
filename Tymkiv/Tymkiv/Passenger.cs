﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tymkiv
{
    public class Passenger
    {
        public int Number_Document { get; set; }

        public int Type_Document { get; set; }

        public string name { get; set; }

        public string surname { get; set; }

        public string address { get; set; }

        public string phone { get; set; }

        public string email { get; set; }

    }
}
