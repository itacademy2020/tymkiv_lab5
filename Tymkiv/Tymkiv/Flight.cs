﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tymkiv
{
    public class Flight
    {
        public int Number_Flight { get; set; }

        public DateTime Date { get; set; }

        public string Place_Dispatch { get; set; }
        
        public string Place_Arrival { get; set; }

        public string Transit_Airfield { get; set; }

        public DateTime Time_Dispatch { get; set; }

        public DateTime Time_Dispatch_Transit { get; set; }

        public DateTime Time_Arrival { get; set; }

        public DateTime Time_Arrival_Transit { get; set; }

        public int Airplane { get; set; }

    }
}
