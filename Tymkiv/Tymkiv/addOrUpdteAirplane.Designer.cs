﻿namespace Tymkiv
{
    partial class addOrUpdteAirplane
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.typeAirplaneComboBox = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.brandTextBox = new System.Windows.Forms.TextBox();
            this.modelTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.flightRangeTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.fuelTextBox = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.tonnageTextBox = new System.Windows.Forms.TextBox();
            this.capacityTextBox = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.accept = new System.Windows.Forms.Button();
            this.cancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Тип літака";
            // 
            // typeAirplaneComboBox
            // 
            this.typeAirplaneComboBox.FormattingEnabled = true;
            this.typeAirplaneComboBox.Location = new System.Drawing.Point(12, 32);
            this.typeAirplaneComboBox.Name = "typeAirplaneComboBox";
            this.typeAirplaneComboBox.Size = new System.Drawing.Size(151, 28);
            this.typeAirplaneComboBox.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 20);
            this.label2.TabIndex = 0;
            this.label2.Text = "Бренд";
            // 
            // brandTextBox
            // 
            this.brandTextBox.Location = new System.Drawing.Point(13, 100);
            this.brandTextBox.Name = "brandTextBox";
            this.brandTextBox.Size = new System.Drawing.Size(150, 27);
            this.brandTextBox.TabIndex = 2;
            this.brandTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stringTextBox_KeyPress);
            // 
            // modelTextBox
            // 
            this.modelTextBox.Location = new System.Drawing.Point(13, 163);
            this.modelTextBox.Name = "modelTextBox";
            this.modelTextBox.Size = new System.Drawing.Size(150, 27);
            this.modelTextBox.TabIndex = 2;
            this.modelTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stringTextBox_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 139);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 20);
            this.label3.TabIndex = 0;
            this.label3.Text = "Модель";
            // 
            // flightRangeTextBox
            // 
            this.flightRangeTextBox.Location = new System.Drawing.Point(13, 227);
            this.flightRangeTextBox.Name = "flightRangeTextBox";
            this.flightRangeTextBox.Size = new System.Drawing.Size(150, 27);
            this.flightRangeTextBox.TabIndex = 2;
            this.flightRangeTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.floatTextBox_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 204);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(137, 20);
            this.label4.TabIndex = 0;
            this.label4.Text = "Дальність польоту";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(189, 204);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 20);
            this.label5.TabIndex = 0;
            this.label5.Text = "Місткість баку";
            // 
            // fuelTextBox
            // 
            this.fuelTextBox.Location = new System.Drawing.Point(190, 227);
            this.fuelTextBox.Name = "fuelTextBox";
            this.fuelTextBox.Size = new System.Drawing.Size(150, 27);
            this.fuelTextBox.TabIndex = 2;
            this.fuelTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.floatTextBox_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(189, 139);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Тоннаж";
            // 
            // tonnageTextBox
            // 
            this.tonnageTextBox.Location = new System.Drawing.Point(190, 163);
            this.tonnageTextBox.Name = "tonnageTextBox";
            this.tonnageTextBox.Size = new System.Drawing.Size(150, 27);
            this.tonnageTextBox.TabIndex = 2;
            this.tonnageTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.floatTextBox_KeyPress);
            // 
            // capacityTextBox
            // 
            this.capacityTextBox.Location = new System.Drawing.Point(190, 100);
            this.capacityTextBox.Name = "capacityTextBox";
            this.capacityTextBox.Size = new System.Drawing.Size(150, 27);
            this.capacityTextBox.TabIndex = 2;
            this.capacityTextBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.intTextBox_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(189, 76);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(141, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Пасажиромісткість";
            // 
            // accept
            // 
            this.accept.Location = new System.Drawing.Point(13, 271);
            this.accept.Name = "accept";
            this.accept.Size = new System.Drawing.Size(344, 40);
            this.accept.TabIndex = 3;
            this.accept.Text = "Підтвердити";
            this.accept.UseVisualStyleBackColor = true;
            this.accept.Click += new System.EventHandler(this.accept_Click);
            // 
            // cancel
            // 
            this.cancel.Location = new System.Drawing.Point(13, 317);
            this.cancel.Name = "cancel";
            this.cancel.Size = new System.Drawing.Size(344, 40);
            this.cancel.TabIndex = 3;
            this.cancel.Text = "Відмінити";
            this.cancel.UseVisualStyleBackColor = true;
            this.cancel.Click += new System.EventHandler(this.cancel_Click);
            // 
            // addOrUpdteAirplane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(369, 384);
            this.Controls.Add(this.cancel);
            this.Controls.Add(this.accept);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.capacityTextBox);
            this.Controls.Add(this.tonnageTextBox);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.fuelTextBox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.flightRangeTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.modelTextBox);
            this.Controls.Add(this.brandTextBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.typeAirplaneComboBox);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "addOrUpdteAirplane";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "addOrUpdteAirplane";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox typeAirplaneComboBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox brandTextBox;
        private System.Windows.Forms.TextBox modelTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox flightRangeTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox fuelTextBox;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox tonnageTextBox;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox capacityTextBox;
        private System.Windows.Forms.Button accept;
        private System.Windows.Forms.Button cancel;
    }
}