﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Tymkiv
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Tables table = new Tables();
            table.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            SqlQuryes sqlQuryes = new SqlQuryes();
            sqlQuryes.ShowDialog();
        }
    }
}
