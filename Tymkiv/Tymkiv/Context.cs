﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tymkiv
{
    class Context
    {
        private Airplanes airplanes;
        private Flights flights;
        private Emploees emploees;
        private Passengers passengers;

        public Airplanes Airplanes {
            private set { }
            get
            {
                return airplanes;
            }
        }

        public Flights Flights
        {
            private set { }
            get
            {
                return flights;
            }
        }

        public Emploees Emploees
        {
            private set { }
            get
            {
                return emploees;
            }
        }

        public Passengers Passengers
        {
            private set { }
            get
            {
                return passengers;
            }
        }

        public Context()
        {
            airplanes = new Airplanes();
            flights = new Flights();
            emploees = new Emploees();
            passengers = new Passengers();
        }
    }
}
